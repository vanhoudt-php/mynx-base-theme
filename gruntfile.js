module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		compass: {
			build: {
				options: {
					outputStyle: 'nested',
					debugInfo: true,
					noLineComments: false,
					httpPath: '',
					httpImagesPath: '../images',
					httpFontsPath: '../fonts',
					sassDir: 'sass',
					cssDir: 'css',
					fontsDir: 'fonts',
					imagesDir: 'images',
					environment: 'development',
					relativeAssets: false,
					force: true
				}
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks('grunt-contrib-compass');

	grunt.registerTask('default', ['compass']);
};
